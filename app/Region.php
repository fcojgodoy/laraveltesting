<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Entity;

class Region extends Model
{
    public function entities()
    {
        return $this->belongsToMany(Entity::class);
    }
}
