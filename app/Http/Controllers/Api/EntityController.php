<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity;
use App\Http\Resources\Entity as EntityResource;
use App\Http\Resources\EntityCollection;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;

class EntityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show']);
        // $this->middleware('auth')->only(['mine', 'store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Entity::paginate();
        return new EntityCollection(Entity::paginate());

        // TIPS
        // return Entity::all();
        // return DB::table('entities')->paginate();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mine()
    {
        return Entity::where('owner_id', auth()->id())->paginate();

        // TIPS: auth() uses
        // auth()->check();
        // auth()->user();
        // auth()->id();
        // auth()->guest();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $entity = request()->validate([
            'name' => 'bail|required',
            'description' => ['required', 'min:3', 'max:687'],
        ]);
        $entity['owner_id'] = auth()->user()->id;
        return Entity::create($entity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {
        // return $entity;
        return new EntityResource($entity);
    }

    /**
     * Display the specified resource for auth user.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function showOwn(Entity $entity)
    {
        // abort_unless(auth()->user()->owns($entity), 403);
        abort_if($entity->owner_id !== auth()->id(), 403);
        return new EntityResource($entity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entity $entity)
    {
        $entity->update(Request(['name', 'description']));
        return $entity;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entity $entity)
    {
        $entity->delete();
        return $entity;
    }

    /**
     * Update the many to many relationship bettwen Entity From Entity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function entitiesFrom(Request $request, Entity $entity)
    {
        $entity->entitiesFrom()->sync($request->toArray());
    }

    /**
     * Update the many to many relationship bettwen Entity To Entity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function entitiesTo(Request $request, Entity $entity)
    {
        $entity->entitiesTo()->sync($request->toArray());
    }

    /**
     * Update the many to many relationship bettwen Category and Entity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function updateCategories(Request $request, Entity $entity)
    {
        $entity->categories()->sync($request->toArray());
    }

    /**
     * Update the many to many relationship bettwen Product and Entity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function updateProducts(Request $request, Entity $entity)
    {
        $entity->products()->sync($request->toArray());
    }

    /**
     * Update the many to many relationship bettwen Product and Regions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function updateRegions(Request $request, Entity $entity)
    {
        $entity->regions()->sync($request->toArray());
    }
}
