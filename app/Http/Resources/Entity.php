<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Entity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $categories = $this->categories->toArray();
        $categories_level_0 = array_filter($categories, function ($category)
        {
            return $category['level'] == 0;
        });
        $categories_level_1 = array_filter($categories, function ($category)
        {
            return $category['level'] == 1;
        });

        $entity = parent::toArray($request);
        unset($entity['categories']);
        $entity['categories_level_0'] = $categories_level_0;
        $entity['categories_level_1'] = $categories_level_1;
        $entity['entities_from'] = $this->entitiesFrom;
        $entity['entities_to'] = $this->entitiesTo;
        $entity['products'] = $this->products;
        $entity['regions'] = $this->regions;
        return $entity;
    }
}
