<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Entity;

class Category extends Model
{
    /**
     * The attributes that can not be mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function entities()
    {
        return $this->belongsToMany(Entity::class);
    }
}
