<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Product;
use App\Region;

class Entity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'slogan', 'description', 'verified_at', 'owner_id', 'region_id', 'contact_address', 'contact_email', 'contact_phone', 'contact_web', 'contact_latlong'
    // ];

    /**
     * The attributes that can not be mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Check if is verified.
     *
     * @return Boolean
     */
    public function isVerified()
    {
        return (bool) $this->verified_at;
    }

    public function entitiesFrom()
    {
        return $this->belongsToMany(Entity::class, 'entity_entity', 'entity_to_id', 'entity_from_id');
    }
    
    public function entitiesTo()
    {
        return $this->belongsToMany(Entity::class, 'entity_entity', 'entity_from_id', 'entity_to_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function regions()
    {
        return $this->belongsToMany(Region::class);
    }
}
