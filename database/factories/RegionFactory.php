<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Region;
use Faker\Generator as Faker;

$factory->define(Region::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'description' => $faker->paragraph(1),
    ];
});
