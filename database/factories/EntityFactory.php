<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Entity;
use App\Region;
use App\User;

$factory->define(Entity::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->paragraph(1),
        'verified_at' => now(),
        'contact_address' => $faker->address,
        'contact_email' => $faker->safeEmail,
        'contact_phone' => $faker->phoneNumber,
        'contact_web' => $faker->url,
        'contact_latlong' => $faker->latitude . ',' . $faker->longitude,
        'owner_id' => User::all()->random()->id,
    ];
});
