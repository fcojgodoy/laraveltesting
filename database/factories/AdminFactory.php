<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => 'Fran',
        'email' => 'fragodoni@gmail.com',
        'email_verified_at' => now(),
        'password' => '1234', // password
        'remember_token' => Str::random(10),
    ];
});
