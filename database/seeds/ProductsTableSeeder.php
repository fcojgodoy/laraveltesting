<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        $json = File::get("database/data/products.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Product::create(array(
            'name' => $obj->name,
            // 'description' => $obj->description,
          ));
        }
    }

}
