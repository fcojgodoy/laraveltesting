<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    // public function run()
    // {
    //     factory(App\User::class, 2)->create();
    // }
    public function run()
    {
        DB::table('users')->delete();
        $json = File::get("database/data/users.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          User::create([
            'email' => $obj->email,
            'name' => $obj->name,
            'password' => encrypt($obj->password),
            'email_verified_at' => now(),
          ]);
        }
    }

}
