<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnersEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('entities')) {
            if (!Schema::hasColumn('entities','owner_id')) {
                Schema::table('entities', function (Blueprint $table) {
                    $table->unsignedBigInteger('owner_id');
                    $table->foreign('owner_id')
                        ->references('id')
                        ->on('users');
                        // TIPS: ->onDelete('cascade') = On delete
                        // users delete the Entities
                });
            }
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('entities')) {
            if (Schema::hasColumn('entities','owner_id')) {
                Schema::table('entities', function (Blueprint $table) {
                    $table->dropColumn('owner_id');
                });
            }
        }
    }
}
