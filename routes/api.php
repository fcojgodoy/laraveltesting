<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'Api\AuthController@login');
Route::post('auth/register', 'Api\AuthController@register');
Route::get('auth/account', 'Api\AuthController@account')->middleware('auth:api');

Route::get('entities/mine', 'Api\EntityController@mine');
Route::get('entities/own/{entity}', 'Api\EntityController@showOwn');
Route::put('entities/{entity}/entities-from', 'Api\EntityController@entitiesFrom');
Route::put('entities/{entity}/entities-to', 'Api\EntityController@entitiesTo');
Route::put('entities/{entity}/categories', 'Api\EntityController@updateCategories');
Route::put('entities/{entity}/products', 'Api\EntityController@updateProducts');
Route::put('entities/{entity}/regions', 'Api\EntityController@updateRegions');

// TIPS: resource after any specific method
Route::apiResources([
    'entities' => 'Api\EntityController',
    'categories' => 'CategoryController',
    'products' => 'ProductController'
]);

// Route::middleware('auth:api')->group(function () {
// });

// Route::fallback(function(){
//     return response()->json([
//         'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
// });
